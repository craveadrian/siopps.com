<div id="services">
	<div class="row">
		<img src="public/images/content/tools.png" alt="tools" class="tools">
		<h5>0.1</h5>
		<h2>Our Services</h2>
		<div class="container">
			<a href="services#content" class="svc_link">
				<img src="public/images/content/service1.jpg" alt="BATHROOM">
				<p>BATHROOM</p>
			</a>
			<a href="services#content" class="svc_link">
				<img src="public/images/content/service2.jpg" alt="KITCHENS">
				<p>KITCHENS</p>
			</a>
			<a href="services#content" class="svc_link">
				<img src="public/images/content/service3.jpg" alt="DECKS">
				<p>DECKS</p>
			</a>
			<a href="services#content" class="svc_link">
				<img src="public/images/content/service4.jpg" alt="AND MUCH MORE">
				<p>AND MUCH<br>MORE</p>
			</a>
		</div>
	</div>
</div>
<div id="welcome">
	<div class="row">
		<img src="public/images/content/welcomeSide.jpg" alt="welcomeSide" class="wlcSide">
		<h5>0.2</h5>
		<h4>Welcome to</h4>
		<h1>Shared Investment <br> Opportunities</h1>
		<div class="container">
			<div class="wlcLeft text-center col-6 fl">
				<img src="public/images/content/welcome.jpg" alt="deck" class="wlcImage">
			</div>
			<div class="wlcRight text-left col-5 fl">
				<p>Shared Investment Opportunities is here to assist you in making you dream home become a reality or just to repair your home.</p>
				<!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse hendrerit ligula quis velit pharetra egestas. Ut molestie elit non massa. Vivamus tincidunt bibendum velit. Quisque nisl diam, nonummy eu, lobortis consequat, mollis ut, tellus. </p>
				<p>Mauris lectus lorem, rhoncus non, tincidunt ut, laoreet ac, augue. Aenean dignissim diam at risus. Aliquam adipiscing elementum lorem. Aliquam ut erat. Donec eget urna eget est euismod imperdiet.</p> -->
				<a href="services#content" class="btn">LEARN MORE</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div id="about">
	<div class="row">
		<h5>0.3</h5>
		<h2>About Us</h2>
		<div class="container">
			<div class="abtLeft text-left col-4 fl">
				<!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse hendrerit ligula quis velit pharetra egestas. Ut molestie elit non massa. Vivamus tincidunt bibendum velit. Quisque nisl diam, nonummy eu, lobortis consequat, mollis ut, tellus. In iaculis. Integer iaculis justo. Duis velit enim, tristique sed, elementum vel, vehicula condimentum, felis. Aenean nisi odio, porta vel, tristique vel, semper mollis, mi.</p>
				<p>Donec gravida, ipsum vitae convallis sodales, urna justo commodo nunc, non venenatis nisl urna non purus. Mauris lectus lorem, rhoncus non, tincidunt ut, laoreet ac, augue. Aenean dignissim diam at risus. Aliquam adipiscing elementum lorem. Aliquam ut erat. Donec eget urna eget est euismod imperdiet.</p> -->
				<p>With over 15 years of experience in the renovation and home buying/rental arenas, Shared Investment Opportunities (SIO) ultimate goal is to put you at ease and ensure customer satisfaction by providing quality service. </p>
				<p>With custom designs we aim to make your renovation dreams come true! We help to provide solutions that meet your need and your budget!</p>
				<p>We also offer assistance with home buying consultation and rental need</p>
				<a href="services#content" class="btn">LEARN MORE</a>
			</div>
			<div class="abtRight text-center col-7 fl">
				<img src="public/images/content/about.jpg" alt="house" class="abtImage">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<h5>0.4</h5>
		<h2>Our Gallery</h2>
		<ul>
			<li><a href="gallery#content">BATHROOMS</a></li>
			<li><a href="gallery#content">KITCHENS</a></li>
			<li><a href="gallery#content">DECKS</a></li>
			<li><a href="gallery#content">AND MORE</a></li>
		</ul>
		<div class="container">
			<div class="galTop">
				<dl>
					<dt> <img src="public/images/content/gallery1.jpg" alt="Before"> </dt>
					<dd class="before">BEFORE</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/gallery2.jpg" alt="After"> </dt>
					<dd class="after">AFTER</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/gallery3.jpg" alt="Before"> </dt>
					<dd class="before">BEFORE</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/gallery4.jpg" alt="After"> </dt>
					<dd class="after">AFTER</dd>
				</dl>
			</div>
			<div class="galBot">
				<dl>
					<dt> <img src="public/images/content/gallery5.jpg" alt="Before"> </dt>
					<dd class="before">BEFORE</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/gallery6.jpg" alt="After"> </dt>
					<dd class="after">AFTER</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/gallery7.jpg" alt="Before"> </dt>
					<dd class="before">BEFORE</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/gallery8.jpg" alt="After"> </dt>
					<dd class="after">AFTER</dd>
				</dl>
			</div>
		</div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<img src="public/images/content/contactSide.jpg" alt="contact image side" class="cntSide">
		<h5>0.5</h5>
		<h2>Contact Us</h2>
		<div class="container">
			<div class="contactLeft text-center col-6 fl">
				<div class="section">
					<ul class="text-left">
						<li><a href="https://www.google.com.ph/maps/place/Stone+Mountain,+GA,+USA/@33.8023882,-84.1909311,14z/data=!3m1!4b1!4m5!3m4!1s0x88f5af24c25d5bbb:0x4119e5c541409adc!8m2!3d33.8081608!4d-84.170196" target="_blank" class="addBot"><?php $this->info("address"); ?></a></li>
						<li><?php $this->info(["phone","tel","phoneBot"]); ?></li>
						<li><?php $this->info(["email","mailto","emailBot"]); ?></li>
						<li> <p class="timebot">Monday - Friday 8am - 5pm</p> </li>
					</ul>
					<p class="accept">WE ACCEPT:</p>
					<img src="public/images/common/cards.png" alt="cards">
				</div>
			</div>
			<div class="contactRight text-right col-6 fl">
				<div class="section">
					<h3>Free Quote</h3>
					<p>Have a question, comment, or compliment?<br>
						Send us a quick message!</p>
					<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
						<label><span class="ctc-hide">Name</span>
							<input type="text" name="name" placeholder="Name:">
						</label>
						<label><span class="ctc-hide">Email</span>
							<input type="text" name="email" placeholder="Email:">
						</label>
						<label><span class="ctc-hide">Message</span>
							<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
						</label>
						<div class="g-recaptcha"></div>
						<label>
							<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
						</label><br>
						<?php if( $this->siteInfo['policy_link'] ): ?>
						<label>
							<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
						</label>
						<?php endif ?>
						<button type="submit" class="ctcBtn btn" disabled>SEND MESSAGE</button>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<img src="public/images/content/tools.png" alt="tools" class="foottools">
</div>
<div id="map">
	<div class="row">
		<a href="<?php echo URL; ?>"> <img src="public/images/common/footLogo.png" alt="Shared Investment Opportunities Main Logo" class="footLogo"></a>
	</div>
</div>
