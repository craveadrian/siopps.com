<div id="content">
  <div class="row">
  	<div id="inner_services">
  		<h1>Services</h1>

		<h2>Services Offered:</h2>
  		<ul>
  			<li>- Kitchen Remodeling</li>
  			<li>- Bathroom Remodeling</li>
  			<li>- Flooring (Tile, Laminate & Hardwood)</li>
  			<li>- Painting</li>
  			<li>- Siding</li>
  			<li>- Plumbing</li>
  			<li>- Electrical</li>
  			<li>- Roofing</li>
  			<li>- Decks</li>
  			<li>- Home Buying Consultations</li>
  			<li>- Home and Room Rental Needs</li>
  			<li>- And more………………</li>
  		</ul>
  		<h2><strong>Our Guarantee:</strong></h2>
  		<ul>
  			<li> - All Work is guaranteed for up to 12 months</li>
  			<li> - Refer a friend and receive $50 thank if they hire us!</li>
  		</ul>
      <div>
        <h2><strong>Contact Us Today!</strong></h2><br>
        <?php $this->info(["phone","tel"]); ?><br><br>
        <span>3636 Juhan Road</span>
        <span><?php $this->info("address"); ?></span>
        <p><strong>Julius Mays – Owner/Operator</strong></p>
      </div>


		<!--end of inner_services-->
  	</div>
  </div>
</div>
